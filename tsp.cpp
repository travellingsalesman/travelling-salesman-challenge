/* 
 * Travelling Salesman Challenge
 *
 * Copyright (C) 2017 Pavel Grunt
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Pavel Grunt <pavlik.grunt@seznam.cz>
 */

#include <iostream>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <utility>
#include <random>
#include <algorithm>
#include <functional>

#include "csv/csv.h"

typedef std::vector<unsigned short> Chromosome;
typedef std::pair<Chromosome, int> EvaluatedChromosome;

/* day -> from -> to -> price */
std::unordered_map<unsigned short, std::unordered_map<std::string,
                   std::unordered_map<std::string, unsigned short> > > schedule;
std::unordered_map<std::string, unsigned short> cities;
std::unordered_map<unsigned short, std::string> city_id_to_str;
std::unordered_set<std::string> city_set;
std::string start_city;

std::random_device rd;
std::mt19937 rng(rd());
int rand_between(int min, int max) {
    std::uniform_int_distribution<int> uni(min,max);
    return uni(rng);
}


bool path_exists(unsigned short day, std::string city_from, std::string city_to) {
    return schedule[day][city_from].find(city_to) != schedule[day][city_from].end();
}


bool path_exists(unsigned short day, unsigned short city_from, unsigned short city_to) {
    return path_exists(day, city_id_to_str[city_from], city_id_to_str[city_to]);
}


std::string random_city(std::unordered_map<std::string, unsigned short> cities_to,
                                           std::unordered_set<std::string> nonvisited) {
    unsigned tries = 100;
    while (tries--) {
        auto random_it = std::next(std::begin(nonvisited), rand_between(0, nonvisited.size() -1));
        if (cities_to.find(*random_it) != cities_to.end())
            return *random_it;
    }
    return std::string("");
}


EvaluatedChromosome random_specimen() {
    bool succ = false;
    while (!succ) {
        int fitness = 0;
        unsigned short day = 0;
        Chromosome specimen;
        std::unordered_set<std::string> nonvisited(city_set);
        std::string city = start_city;

        succ = true;
        nonvisited.erase(city);

        for (day = 0; day < schedule.size() - 1; day++) {
            std::string rnd_city = random_city(schedule[day][city], nonvisited);
            if (rnd_city.empty()) {
                succ = false;
                nonvisited.clear();
                specimen.clear();
                break;
            }
            unsigned short bb = cities.at(rnd_city);
            specimen.push_back(bb);
            fitness += schedule[day][city][rnd_city];
            city = rnd_city;
            nonvisited.erase(city);
        }
        succ = path_exists(day, city, start_city);
        if (succ) {
            nonvisited.clear();
            fitness += schedule[day][city][start_city];
            return std::make_pair(specimen, fitness);
        }
    }
    return std::make_pair(std::vector<unsigned short>(), -1);
}


int evaluate(Chromosome specimen) {
    int fitness = 0;
    unsigned short day;
    std::string city_from = start_city;

    for (day = 0; day < specimen.size() ; ++day) {
        std::string city_to = city_id_to_str[specimen[day]];
        fitness += schedule[day][city_from][city_to];
        city_from = city_to;
    }
    fitness += schedule[day][city_from][start_city];

    return fitness;
}


int evaluate(EvaluatedChromosome specimen) {
    return evaluate(specimen.first);
}


bool is_valid(Chromosome specimen) {
    unsigned short day;
    std::string city_from = start_city;

    for (day = 0; day < specimen.size() ; ++day) {
        std::string city_to = city_id_to_str[specimen[day]];
        if (!path_exists(day, city_from, city_to)) {
#ifdef DEBUG
            std::cerr << "NO PATH from " << city_from << " => " << city_to;
            std::cerr << " on " << day << std::endl;
#endif
            return false;
        }
        city_from = city_to;
    }

    return path_exists(day, city_from, start_city);
}


bool is_valid(EvaluatedChromosome specimen) {
    return is_valid(specimen.first);
}


void swap_cities(Chromosome specimen, int a, int b) {
    int tmp_swap;
    tmp_swap = specimen[a];
    specimen[a] = specimen[b];
    specimen[b] = tmp_swap;
}


void mutate_swap(Chromosome specimen) {
    int max_index = specimen.size() - 1;
    bool valid_specimen = false;
    while (!valid_specimen) {
        swap_cities(specimen, rand_between(0, max_index), rand_between(0, max_index));
        valid_specimen = is_valid(specimen);
    }
}


void mutate_swap(EvaluatedChromosome specimen) {
    mutate_swap(specimen.first);
    specimen.second = evaluate(specimen);
}


void mutate_inverse(Chromosome specimen) {
    int max_index = specimen.size() - 1;
    bool valid_specimen = false;
    while (!valid_specimen) {
        int a, b;
        a = rand_between(0, max_index);
        b = rand_between(0, max_index);
        if (b < a) {
            std::swap(a, b);
        }
        while (a < b) {
            swap_cities(specimen, a, b);
            ++a;
            --b;
        }
        valid_specimen = is_valid(specimen);
    }
}


void mutate_inverse(EvaluatedChromosome specimen) {
    mutate_inverse(specimen.first);
    specimen.second = evaluate(specimen);
}


int get_price(unsigned short day, std::string city_from, std::string city_to) {
    if (!path_exists(day, city_from, city_to)) {
        return -1;
    }
    return schedule[day][city_from][city_to];
}


int get_price(unsigned short day, unsigned short city_from, unsigned short city_to) {
    return get_price(day, city_id_to_str[city_from], city_id_to_str[city_to]);
}


Chromosome crossover_SCX(Chromosome a, Chromosome b) {
    unsigned short i, day, index;
    Chromosome child;
    child.reserve(a.size());
    std::unordered_map<unsigned short, std::pair<int, int> > neighbourmap;
    std::unordered_set<unsigned short> unused_cities(a.begin(), a.end()); 

    // std::cerr << unused_cities.size() << std::endl;
    for (auto & aa : a) {
        neighbourmap[aa] = std::make_pair<int, int>(-1, -1);
        // std::cerr << aa << '\t' << city_id_to_str[aa] << std::endl;
    }


    for (i = 0; i < a.size() - 1; ++i) {
        // std::cerr << i << '\t' << a[i] << '\t' << a[i+1] << std::endl;
        neighbourmap[a[i]].first = a[i+1];
        neighbourmap[b[i]].second = b[i+1];
    }

#ifdef DEBUG
    std::cerr << "nsize " << neighbourmap.size() << std::endl;
    for (auto & n : neighbourmap) {
        std::cerr << n.first << '\t' << n.second << std::endl;        
    }
#endif

    /* create a new chromosome */
    index = a[0];
    for (day = 1; day < a.size() + 1; day++) {
        child.push_back(index);
        if (day >= a.size()) {
            break;
        }
    // std::cerr << day << " - " << index << " : " << unused_cities.size() << std::endl;
        unused_cities.erase(index);
    // std::cerr << unused_cities.size() << std::endl;
        for (unsigned short j = 0; j < neighbourmap.size(); j++) {
            if (neighbourmap[j].first == index) {
                neighbourmap[j].first = -1;
            }
            if (neighbourmap[j].second == index) {
                neighbourmap[j].second = -1;
            }
        }
        if ((neighbourmap[index].first < 0 || neighbourmap[index].second < 0) ||
            (!path_exists(day, index, neighbourmap[index].first) &&
             !path_exists(day, index, neighbourmap[index].first))) {
            std::vector<unsigned short> unused(unused_cities.begin(), unused_cities.end());
            unsigned short min;
            int min_price = -1;
            for (min = 0; min < unused.size(); ++min) {
                min_price = get_price(day, index, unused[min]);
                if (min_price >= 0) {
                    break;
                }
            }
            if (min_price == -1) {
#ifdef DEBUG
                std::cerr << " error in SCX" << std::endl;
#endif
                return a;
            }
            for (unsigned short j = min + 1; j < unused.size(); ++j) {
                int p1 = get_price(day, index, unused[j]);
                if (p1 < 0) {
                    continue;
                }
                if (p1 < min_price) {
                    min = j;
                    min_price = p1;
                }
            }
            // std::cerr <<"  min " << min << " " << unused_cities.size() << std::endl;
            index = unused[min];
        } else {
            int p1 = get_price(day, index, neighbourmap[index].first);
            int p2 = get_price(day, index, neighbourmap[index].second);
            if (p1 != -1 && p1 < p2) {
                index = neighbourmap[index].first;
            } else if (p2 != -1) {
                index = neighbourmap[index].second;                
            } else {
                return a;
            }
        }
    }

    if (is_valid(child))
        return child;
#ifdef DEBUG
    for (auto &x : child) {
        std::cerr << city_id_to_str[x] << ", ";
    }
    std::cerr << " error in SCX" << std::endl;
#endif
    return a;
}


EvaluatedChromosome crossover_SCX(EvaluatedChromosome a, EvaluatedChromosome b) {
    EvaluatedChromosome child;
    child.first = crossover_SCX(a.first, b.first);
    child.second = evaluate(child.first);
    return child;
}

int main(void) {
    std::string city_from, city_to;
    unsigned short max_day = 0, day, price, index = 0;

    if (!getline(std::cin, start_city)) { /* no input */
        std::cerr << "No input from STDIN" << std::endl;
        return 1;
    }

    io::CSVReader<4, io::trim_chars<'\t'>, io::no_quote_escape<' '>> csv_reader("", std::cin);
    while (csv_reader.read_row(city_from, city_to, day, price)) {
        max_day = day > max_day ? day : max_day;
        /* only the first day we travel from the start city */
        if (day == 0 && city_from != start_city) {
            continue;
        }
        if (day > 0 && city_from == start_city) {
            /* on the first day we travel only from the start city */
            continue;
        }
        if (day < max_day && city_to == start_city) {
            /* we want to travel to the start city only on the last day */
            continue;
        }
        schedule[day][city_from][city_to] = price;
        city_set.insert(city_from);
    }

    /* create helper maps */
    for (auto &c : city_set) {
        cities[c] = index;
        city_id_to_str[index] = c;
        index++;
    }

#ifdef DEBUG
    for (day = 0; day < schedule.size(); day++) {
        std::cerr << day << std::endl;
        for (auto &ft : schedule[day]) {
            for (auto &tp : ft.second){
                std::cerr << '\t' << ft.first << " => " << tp.first << std::endl;
            }
        }
    }
#endif

    unsigned short generations = 100;
    const unsigned short population_size = 100;
    EvaluatedChromosome specimen, best_specimen;
    std::vector<EvaluatedChromosome> population;

    /* generate initial population */
    for (unsigned short i = 0; i < population_size; i++) {
        specimen = random_specimen();
        population.push_back(specimen);
    }
    std::sort(population.begin(), population.end(),
             [](std::pair<std::vector<unsigned short>, int> a,
                std::pair<std::vector<unsigned short>, int> b) {
        return a.second < b.second;   
    });
    best_specimen = population[0];

#ifdef DEBUG
    std::cerr << best_specimen.second << std::endl;
#endif

    while (generations--) {
        for (unsigned short i = 0; i < population_size; i++) {
            switch (i % 4) {
            case 1:
                specimen = EvaluatedChromosome(population[i]);
                mutate_swap(specimen);
                population.push_back(specimen);
                break;
            case 2:
                specimen = EvaluatedChromosome(population[i]);
                mutate_inverse(population[i]);
                population.push_back(specimen);
                break;
            case 3:
                population.push_back(crossover_SCX(population[i], population[population_size - i]));
                break;
            default:
                population.push_back(random_specimen());
                break;
            }
        }
        std::sort(population.begin(), population.end(),
                  [](std::pair<std::vector<unsigned short>, int> a,
                     std::pair<std::vector<unsigned short>, int> b) {
            return a.second < b.second;   
        });
        specimen = population[0];
        population.resize(population_size);

        population[population_size - 1] = best_specimen;
        if (specimen.second < best_specimen.second) {
            best_specimen = specimen;
        }
#ifdef DEBUG
        std::cerr << best_specimen.second << std::endl;
#endif        
    }

    std::cout << best_specimen.second << std::endl;
    city_from = start_city;
    for (day = 0; day < best_specimen.first.size(); ++day) {
        city_to = city_id_to_str[best_specimen.first[day]];
        std::cout << city_from << " " << city_to << " " << day << " " << schedule[day][city_from][city_to] << std::endl;
        city_from = city_to;
    }
    city_to = start_city;
    std::cout << city_from << " " << city_to << " " << day << " " << schedule[day][city_from][city_to] << std::endl;

    return 0;
}
