extern crate csv;
extern crate fnv;
extern crate string_interner;
extern crate rand;
extern crate time;
extern crate num_cpus;
extern crate crossbeam;

use std::io;
use std::mem;

use fnv::{FnvHashMap, FnvHashSet};
use rand::{thread_rng, sample};
use string_interner::DefaultStringInterner;
use time::Duration;


type CityPrice = FnvHashMap<u16, u16>;
type FromToPrice = FnvHashMap<u16, CityPrice>;
type TripMap = FnvHashMap<u16, FromToPrice>;

type Chromosome = Vec<u16>;
type Population = [Vec<Chromosome>; 2];
type Solution = (Chromosome, u32);

const TRIES: usize = 5; // tries to avoid the endless loops
const MIN_POPULATION_SIZE: u16 = 40;
const MAX_POPULATION_SIZE: u16 = 100;
const TOURNAMENT_SIZE: usize = 5;


#[inline(always)]
fn get_destinations(paths: &TripMap, day: u16, from: u16) -> Option<&CityPrice> {
    paths.get(&day).unwrap().get(&from)
}

#[inline]
fn path_exists(paths: &TripMap, day: u16, from: u16, to: u16) -> bool {
    match get_destinations(paths, day, from) {
        Some(dests) => dests.contains_key(&to),
        None => false,
    }
}

#[inline]
fn get_price(paths: &TripMap, day: u16, from: u16, to: u16) -> Option<u16> {
    if let Some(real_paths) = get_destinations(paths, day, from) {
        return match real_paths.get(&to) {
            Some(&price) => Some(price),
            None => None,
        };
    }
    None
}

fn is_valid(paths: &TripMap, start_city: u16, specimen: &Chromosome) -> bool {
    let mut city = start_city;
    let mut day: u16 = 0;

    for next_city in specimen {
        if !path_exists(paths, day, city, *next_city) {
            return false;
        }
        city = *next_city;
        day = day + 1;
    }
    path_exists(paths, day, city, start_city)
}

fn evaluate(route: &Chromosome, start_city: u16, paths: &TripMap) -> u32 {
    let mut city = start_city;
    let mut fitness: u32 = 0;
    let mut day: u16 = 0;

    for next_city in route {
        fitness = fitness + get_price(paths, day, city, *next_city).unwrap() as u32;
        day = day + 1;
        city = *next_city;
    }
    fitness + get_price(paths, day, city, start_city).unwrap() as u32
}

fn print_itinerary(route: &Chromosome,
                   best_fitness: u32,
                   start_city: u16,
                   paths: &TripMap,
                   cities: &DefaultStringInterner) {
    let mut city = start_city;
    let mut day: u16 = 0;

    println!("{}", best_fitness);

    for next_city in route {
        println!("{} {} {:?} {:?}",
                 cities.resolve(city as usize).unwrap(),
                 cities.resolve(*next_city as usize).unwrap(),
                 day,
                 get_price(paths, day, city, *next_city).unwrap());
        day = day + 1;
        city = *next_city;
    }
    println!("{} {} {:?} {:?}",
             cities.resolve(city as usize).unwrap(),
             cities.resolve(start_city as usize).unwrap(),
             day,
             get_price(paths, day, city, start_city).unwrap());
}

fn random_city(destinations: &CityPrice, notvisited: &FnvHashSet<u16>) -> Option<u16> {
    let mut rng = thread_rng();
    let valid_dests = notvisited.iter().filter(|x| destinations.contains_key(x));
    match sample(&mut rng, valid_dests, 1).pop() {
        Some(&expr) => Some(expr),
        None => None,
    }
}

fn random_path_ip(paths: &TripMap,
                  all_cities: &FnvHashSet<u16>,
                  start_city: u16,
                  max_day: u16,
                  path: &mut Chromosome) {
    loop {
        let mut notvisited: FnvHashSet<u16> = all_cities.clone();
        notvisited.remove(&start_city);
        let mut city = start_city;
        let mut succ = true;
        for day in 0..max_day {
            if let Some(destinations) = get_destinations(paths, day, city) {
                if let Some(next_city) = random_city(&destinations, &notvisited) {
                    city = next_city;
                    path[day as usize] = city;
                    notvisited.remove(&city);
                } else {
                    succ = false;
                    break;
                }
            } else {
                succ = false;
                break;
            }
        }
        if succ && path_exists(paths, max_day, city, start_city) {
            break;
        }
    }
}

fn random_path(paths: &TripMap,
               all_cities: &FnvHashSet<u16>,
               start_city: u16,
               max_day: u16)
               -> Chromosome {
    let mut path: Chromosome = (0..max_day).collect();
    random_path_ip(paths, all_cities, start_city, max_day, &mut path);
    path
}

fn cheapest_destination(paths: &TripMap,
                        day: u16,
                        from: u16,
                        notvisited: &FnvHashSet<u16>,
                        excludes: &FnvHashSet<u16>)
                        -> Option<u16> {
    if let Some(destinations) = get_destinations(paths, day, from) {
        let price_to_dest = |&x| destinations.get(&x).unwrap();
        let mut valid_dests: Vec<&u16> = notvisited.iter()
            .filter(|x| destinations.contains_key(x) && !excludes.contains(x))
            .collect();
        valid_dests.sort_by(|&a, &b| price_to_dest(b).cmp(&price_to_dest(a)));
        if let Some(&city) = valid_dests.pop() {
            return Some(city);
        }
    }
    None
}

fn cheapest_available_first(paths: &TripMap,
                            all_cities: &FnvHashSet<u16>,
                            start_city: u16,
                            max_day: u16)
                            -> Chromosome {
    let mut path: Chromosome = Vec::with_capacity(max_day as usize);
    let mut notvisited: FnvHashSet<u16> = all_cities.clone();
    notvisited.remove(&start_city);
    let mut day = 0;
    let mut excludes: Vec<FnvHashSet<u16>> = Vec::with_capacity(max_day as usize);
    for _ in 0..max_day {
        excludes.push(FnvHashSet::default());
    }

    let mut city = start_city;
    while day < max_day {
        if day == 0 {
            city = start_city;
        }
        if let Some(exclude_set) = excludes.get_mut(day as usize) {
            if let Some(dest) = cheapest_destination(paths, day, city, &notvisited, &exclude_set) {
                city = dest;
                exclude_set.insert(city);
                notvisited.remove(&city);
                path.push(city);
                day = day + 1;
                if day == max_day && !path_exists(paths, day, city, start_city) {
                    day = day - 1;
                    notvisited.insert(path.remove(day as usize));
                    day = day - 1;
                    city = path.remove(day as usize);
                    notvisited.insert(city);
                }
            } else {
                exclude_set.clear();
                day = day - 1;
                city = path.remove(day as usize);
                notvisited.insert(city);
            }
        }
    }

    path
}

fn inverse_path_ip(child: &mut Chromosome,
                   ai: usize,
                   bi: usize,
                   paths: &TripMap,
                   start_city: u16) -> bool {
    let mut a = ai;
    let mut b = bi;
    if b < a {
        mem::swap(&mut a, &mut b);
    }
    while a < b {
        child.swap(a, b);
        a = a + 1;
        b = b - 1;
    }
    is_valid(paths, start_city, &child)
}

fn mutate_inverse_ip(parent: &Chromosome,
                     paths: &TripMap,
                     start_city: u16,
                     child: &mut Chromosome) {
    let mut rng = thread_rng();
    child.copy_from_slice(&parent.as_slice());
    for _ in 0..TRIES {
        let mut samples = sample(&mut rng, 0..parent.len(), 2);
        let a = samples.pop().unwrap();
        let b = samples.pop().unwrap();
        if inverse_path_ip(child, a, b, paths, start_city) {
            return;
        }
        child.copy_from_slice(&parent.as_slice());
    }
}

fn mutate_inverse(parent: &Chromosome, paths: &TripMap, start_city: u16) -> Chromosome {
    let mut child: Chromosome = parent.to_vec();
    mutate_inverse_ip(parent, paths, start_city, &mut child);
    child
}

fn mutate_swap_ip(parent: &Chromosome, paths: &TripMap, start_city: u16, child: &mut Chromosome) {
    let mut rng = thread_rng();
    child.copy_from_slice(&parent.as_slice());
    for _ in 0..TRIES {
        let mut samples = sample(&mut rng, 0..parent.len(), 2);
        let a = samples.pop().unwrap();
        let b = samples.pop().unwrap();
        child.swap(a, b);
        if is_valid(paths, start_city, &child) {
            break;
        }
        child.swap(a, b);
    }
}

fn mutate_swap(parent: &Chromosome, paths: &TripMap, start_city: u16) -> Chromosome {
    let mut child: Chromosome = parent.to_vec();
    mutate_swap_ip(parent, paths, start_city, &mut child);
    child
}

fn crossover_scx_ip(parent_a: &Chromosome,
                    parent_b: &Chromosome,
                    paths: &TripMap,
                    all_cities: &FnvHashSet<u16>,
                    start_city: u16,
                    max_day: u16,
                    child: &mut Chromosome) {
    let mut neighbourmap: [FnvHashMap<u16, u16>; 2] = [FnvHashMap::default(),
                                                       FnvHashMap::default()];
    let mut index = parent_a[0];
    let mut notvisited: FnvHashSet<u16> = all_cities.clone();
    notvisited.remove(&start_city);

    for day in 0..(max_day - 1) {
        neighbourmap[0].entry(parent_a[day as usize]).or_insert(parent_a[(day + 1) as usize]);
        neighbourmap[1].entry(parent_b[day as usize]).or_insert(parent_b[(day + 1) as usize]);
    }

    for day in 1..(max_day + 1) {
        child[(day - 1) as usize] = index;
        if day >= max_day {
            break;
        }

        notvisited.remove(&index);
        for i in 0..(max_day + 1) {
            for j in 0..2 {
                if let Some(&city) = neighbourmap[j].get(&i) {
                    if city == index {
                        neighbourmap[j].remove(&i);
                    }
                }
            }
        }

        let n1 = neighbourmap[0].get(&index);
        let n2 = neighbourmap[1].get(&index);

        if (n1.is_none() || n2.is_none()) ||
           (!path_exists(paths, day, index, *n1.unwrap()) &&
            !path_exists(paths, day, index, *n2.unwrap())) {
            let mut best = None;
            let mut min_price = None;
            for unused_city in notvisited.iter() {
                if let Some(price) = get_price(paths, day, index, *unused_city) {
                    if !min_price.is_some() || price < min_price.unwrap() {
                        best = Some(unused_city);
                        min_price = Some(price);
                    }
                }
            }
            if !best.is_some() {
                child.copy_from_slice(&parent_b.as_slice());
                return;
            }
            index = *best.unwrap();
        } else {
            let n1 = *n1.unwrap();
            let n2 = *n2.unwrap();
            let p1 = get_price(paths, day, index, n1);
            let p2 = get_price(paths, day, index, n2);
            if p1.is_some() {
                if !p2.is_some() || p1.unwrap() < p2.unwrap() {
                    index = n1;
                } else {
                    index = n2;
                }
            } else if p2.is_some() {
                index = n2;
            } else {
                child.copy_from_slice(&parent_b.as_slice());
                return;
            }
        }
    }

    if !is_valid(paths, start_city, &child) {
        child.copy_from_slice(&parent_b.as_slice());
    }
}

fn crossover_scx(parent_a: &Chromosome,
                 parent_b: &Chromosome,
                 paths: &TripMap,
                 all_cities: &FnvHashSet<u16>,
                 start_city: u16,
                 max_day: u16)
                 -> Chromosome {
    let mut child: Chromosome = (0..max_day).collect();
    crossover_scx_ip(parent_a,
                     parent_b,
                     paths,
                     all_cities,
                     start_city,
                     max_day,
                     &mut child);
    child
}

fn tournament_selection<'a>(population: &'a Vec<Chromosome>,
                            start_city: u16,
                            paths: &TripMap,
                            tournament_size: usize)
                            -> &'a Chromosome {
    let mut rng = thread_rng();
    let participants = sample(&mut rng, population.iter(), tournament_size);
    let mut best_id = 0;
    let mut best_fitness = evaluate(participants[best_id], start_city, paths);
    for i in 1..tournament_size {
        let curr_fitness = evaluate(participants[i], start_city, paths);
        if curr_fitness < best_fitness {
            best_id = i;
            best_fitness = curr_fitness;
        }
    }
    participants[best_id]
}

fn run_ga(trips_cost: &TripMap,
          all_cities: &FnvHashSet<u16>,
          start_id: u16,
          max_day: u16,
          population_size: usize,
          max_ga_time: &time::Timespec,
          max_time: &time::Timespec)
          -> Solution {
    let tournament_size: usize = std::cmp::max(population_size / 30, TOURNAMENT_SIZE);
    let random_in_generation: usize = population_size / 10;
    let crossover_in_generation: usize = population_size / 2 + random_in_generation;
    let mut1_in_generation: usize = population_size / 4 + crossover_in_generation;
    let mut best: Chromosome;
    let mut best_fitness: u32;
    let mut parent_population = 0;
    let mut children_population = 1;
    let mut populations: Population = [Vec::with_capacity(population_size),
                                       Vec::with_capacity(population_size)];
    // generate initial population
    best = cheapest_available_first(&trips_cost, &all_cities, start_id, max_day);
    best_fitness = evaluate(&best, start_id, &trips_cost);
    populations[parent_population].push(best.to_vec());
    for _ in 1..population_size {
        let a_random_path = random_path(&trips_cost, &all_cities, start_id, max_day);
        let fitness = evaluate(&a_random_path, start_id, &trips_cost);
        populations[parent_population].push(a_random_path.to_vec());
        if fitness < best_fitness {
            best = a_random_path.to_vec();
            best_fitness = fitness;
        }
    }
    populations[children_population] = populations[parent_population].to_vec();

    while time::get_time() < *max_ga_time {
        populations[children_population][0] = best.to_vec();
        for i in 1..population_size {
            let child = if i < random_in_generation {
                random_path(&trips_cost, &all_cities, start_id, max_day)
            } else if i < crossover_in_generation {
                let parent_a = tournament_selection(&populations[parent_population],
                                                    start_id,
                                                    &trips_cost,
                                                    tournament_size);
                let parent_b = tournament_selection(&populations[parent_population],
                                                    start_id,
                                                    &trips_cost,
                                                    tournament_size);
                crossover_scx(parent_a,
                              parent_b,
                              &trips_cost,
                              &all_cities,
                              start_id,
                              max_day)
            } else if i < mut1_in_generation {
                let parent = tournament_selection(&populations[parent_population],
                                                  start_id,
                                                  &trips_cost,
                                                  tournament_size);
                mutate_swap(parent, &trips_cost, start_id)
            } else {
                let parent = tournament_selection(&populations[parent_population],
                                                  start_id,
                                                  &trips_cost,
                                                  tournament_size);
                mutate_inverse(parent, &trips_cost, start_id)
            };
            let fitness = evaluate(&child, start_id, &trips_cost);
            populations[children_population][i] = child.to_vec();
            if fitness < best_fitness {
                best = child;
                best_fitness = fitness;
            }
        }
        mem::swap(&mut parent_population, &mut children_population);
    }
    return full_two_opt_timed(&best, start_id, &trips_cost, max_time);
}

fn main() {
    let max_time = time::get_time() + Duration::milliseconds(29500);

    let stdin = io::stdin();

    let mut start_city = String::new();
    let mut max_day: u16 = 0;

    let mut cities = DefaultStringInterner::new();
    let mut all_cities: FnvHashSet<u16> = FnvHashSet::default();
    let mut trips_cost: TripMap = FnvHashMap::default();

    stdin.read_line(&mut start_city).unwrap();
    start_city = start_city.trim().to_string();
    let start_id = cities.get_or_intern(start_city) as u16;

    let mut rdr = csv::Reader::from_reader(stdin).has_headers(false).delimiter(b' ');
    while !rdr.done() {
        let from = cities.get_or_intern(rdr.next_str().unwrap()) as u16;
        let to = cities.get_or_intern(rdr.next_str().unwrap()) as u16;
        let day = rdr.next_str().unwrap().parse().unwrap();
        let cost = rdr.next_str().unwrap().parse().unwrap();
        if day > max_day {
            max_day = day;
        }
        match rdr.next_str() {
            csv::NextField::EndOfRecord => (),
            field => panic!("unexpected csv field: {:?}", field),
        }

        // only on the first day we travel from the start city
        if (day == 0 && from != start_id) || (day > 0 && from == start_id) {
            continue;
        }
        // we want to travel to the start city only on the last day
        if day < max_day && to == start_id {
            continue;
        }

        all_cities.insert(from);
        trips_cost.entry(day)
            .or_insert(FnvHashMap::default())
            .entry(from)
            .or_insert(FnvHashMap::default())
            .entry(to)
            .or_insert(cost);
    }

    // remove paths on 1 but last day leading to cities from which the start city is unreachable
    let mut dead_ends: Vec<u16> = Vec::new();
    for &city in all_cities.iter() {
        if !path_exists(&trips_cost, max_day, city, start_id) {
            dead_ends.push(city);
        }
    }
    for &city in all_cities.iter() {
        for &dead in dead_ends.iter() {
            if path_exists(&trips_cost, max_day - 1, city, dead) {
                trips_cost.get_mut(&(max_day - 1)).unwrap().get_mut(&city).unwrap().remove(&dead);
            }
        }
    }

    let two_opt_time = if max_day > 200 {
        Duration::seconds(7)
    } else if max_day > 100 {
        Duration::seconds(4)
    } else {
        Duration::seconds(1)
    };
    let max_ga_time = max_time - two_opt_time;
    let max_threads = 4;
    let mut base_size = std::cmp::min(std::cmp::max(max_day, MIN_POPULATION_SIZE),
                                      MAX_POPULATION_SIZE);
    let base_size_dec = base_size / max_threads;
    base_size += base_size_dec;

    // to avoid copying into crossbeam scope otherwise `move` would take them too
    let trips_cost_ref = &trips_cost;
    let all_cities_ref = &all_cities;

    let mut thread_handles: Vec<crossbeam::ScopedJoinHandle<Solution>> =
        Vec::with_capacity(max_threads as usize);
    crossbeam::scope(|scope| for i in 0..max_threads {
        let population_size = base_size - i * base_size_dec;
        let handle = scope.spawn(move || {
            run_ga(trips_cost_ref,
                   all_cities_ref,
                   start_id,
                   max_day,
                   population_size as usize,
                   &max_ga_time,
                   &max_time)
        });
        thread_handles.push(handle);
    });

    let mut best: Chromosome = Vec::new();
    let mut best_fitness: u32 = std::u32::MAX;
    for handle in thread_handles {
        let (best_next, best_fitness_next): Solution = handle.join();
        if best_fitness_next < best_fitness {
            best = best_next;
            best_fitness = best_fitness_next;
        }
    }

    print_itinerary(&best, best_fitness, start_id, &trips_cost, &cities);
}



// 2opt taken from wiki https://en.wikipedia.org/wiki/2-opt
// just one step - can be used recursively and/or as a mutation
fn two_opt_step(path: &Chromosome,
                start_city: u16,
                trips_cost: &TripMap) -> Solution {
    let mut child: Chromosome = path.to_vec();
    let mut i: usize = 0;
    //Used to hold the tour length of the current solution
    let mut current_distance = evaluate(&path, start_city, trips_cost);
    let path_length = path.len();

    while i < path_length - 1 {
        let mut j = i + 1;
        while j < path_length {
            child.copy_from_slice(&path.as_slice());
            if inverse_path_ip(&mut child, i, j, trips_cost, start_city) {
                let new_distance = evaluate(&child, start_city, trips_cost);
                if new_distance < current_distance {
                    current_distance = new_distance;
                    i = path_length;
                    break;
                }
            }
            j += 1;
        }
        i += 1;
    }

    (child, current_distance)
}

// runs two_opt_step till there is no improvement
fn full_two_opt(path: &Chromosome,
                start_city: u16,
                trips_cost: &TripMap) -> Solution {
    let mut best = path.to_vec();
    let mut best_f2opt = evaluate(&path, start_city, &trips_cost);
    loop {
        let (c, f2opt) = two_opt_step(&best, start_city, &trips_cost);        
        if best_f2opt == f2opt { // no improvement
            break;
        }
        best.copy_from_slice(&c.as_slice());
        best_f2opt = f2opt;
    }
    (best, best_f2opt)
}

// runs two_opt_step till there is no improvement or till it runs out of time
fn full_two_opt_timed(path: &Chromosome,
                      start_city: u16,
                      trips_cost: &TripMap,
                      max_time: &time::Timespec) -> Solution {
    let mut best = path.to_vec();
    let mut best_f2opt = evaluate(&path, start_city, &trips_cost);
    while time::get_time() < *max_time {
        let (c, f2opt) = two_opt_step(&best, start_city, &trips_cost);        
        if best_f2opt == f2opt { // no improvement
            break;
        }
        best.copy_from_slice(&c.as_slice());
        best_f2opt = f2opt;
    }
    (best, best_f2opt)
}

// runs up to TRIES two_opt_step
fn full_two_opt_mutate(path: &Chromosome,
                       start_city: u16,
                       trips_cost: &TripMap) -> Solution {
    let mut best = path.to_vec();
    let mut best_f2opt = evaluate(&path, start_city, &trips_cost);
    for _ in 0..TRIES {
        let (c, f2opt) = two_opt_step(&best, start_city, &trips_cost);        
        if best_f2opt == f2opt { // no improvement
            break;
        }
        best.copy_from_slice(&c.as_slice());
        best_f2opt = f2opt;
    }
    (best, best_f2opt)
}
