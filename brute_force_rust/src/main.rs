extern crate memmap;
extern crate regex;

use memmap::{Mmap, Protection};
use std::collections::HashMap;
use std::str;
use std::iter::FromIterator;
use std::collections::HashSet;
use std::env;
use regex::Regex;
use std::io;
use std::io::Write;

extern crate permutohedron;
use permutohedron::heap_recursive;

type CityToCityDayKey = (String, String, u32);
type TripMap = HashMap<CityToCityDayKey, u32>;

fn calculate_route_cost(origin_city: &str,
                        cities_order: &Vec<&str>,
                        trips_cost: &TripMap)
                        -> Option<u32> {

    let mut cost_sum: u32 = 0;
    let mut current_day: u32 = 0;
    let mut cities_iter = cities_order.iter();
    let mut previous_city = origin_city;
    for city in cities_iter {
        match trips_cost.get(&(previous_city.to_string(), city.to_string(), current_day)) {
            Some(cost) => {
                cost_sum += *cost;
                previous_city = city;
                current_day += 1;
            }
            None => return None,
        }
    }
    Some(cost_sum)
}

fn calculate_best_route_cost(origin_city: &str, trips_cost: &TripMap) {
    let cities: HashSet<&str> = trips_cost.keys().map(|&(ref c1, _, _)| c1.as_str()).collect();
    print!("### citties = {:?}\n", cities);

    let mut cities_vec: Vec<_> = cities.into_iter().filter(|&c| c != origin_city).collect();
    let mut lowest_cost: u32 = std::u32::MAX;
    let mut lowest_cities_route = Vec::new();
    heap_recursive(&mut cities_vec, |permutation| {
        let mut cities_order: Vec<_> = permutation.to_vec();
        cities_order.push(origin_city);

        match calculate_route_cost(&origin_city, &cities_order, &trips_cost) {
            Some(num) => {
                if num < lowest_cost {
                    lowest_cost = num;
                    lowest_cities_route = cities_order;

                    println!("route = {:?}, cost = {}, best = {}",
                             &lowest_cities_route,
                             num,
                             lowest_cost);
                }
            }
            None => {}
        };
    });
    lowest_cities_route.insert(0, origin_city);
    println!("best route = {:?}, cost = {}",
             lowest_cities_route,
             lowest_cost);
}


fn main() {

    let stdout = io::stdout();
    let mut stdout = stdout.lock();

    // let input_file = env::args().nth(1).expect("Provide patch to input file as first param");
    let input_file = "/home/jsilhan/Desktop/travelling-salesman-challenge/rules/real_data/cut_data/data_15.txt";
    let file = Mmap::open_path(input_file, Protection::Read).unwrap();
    // Unsafe because we must guarantee that the file is not concurrently modified.
    let bytes = unsafe { file.as_slice() };
    let s = str::from_utf8(bytes).unwrap();

    let re = Regex::new(r"(\S+) (\S+) (\d+) (\d+)").unwrap();
    let mut trips_cost: HashMap<CityToCityDayKey, u32> = HashMap::new();

    let mut lines = s.lines();
    let origin_city = lines.next().unwrap();

    for line in lines {
        let caps = re.captures(line).unwrap();

        let c1: String = caps.get(1).unwrap().as_str().to_string();
        let c2: String = caps.get(2).unwrap().as_str().to_string();
        let day: u32 = caps.get(3).unwrap().as_str().parse().unwrap();
        let cost: u32 = caps.get(4).unwrap().as_str().parse().unwrap();

        trips_cost.insert((c1, c2, day), cost);
    }

    calculate_best_route_cost(origin_city, &trips_cost);
}
