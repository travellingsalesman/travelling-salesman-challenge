# delete all data
MATCH (n)
OPTIONAL MATCH (n)-[r]-()
DELETE n,r

# load csv
LOAD CSV FROM "file:///data_5.txt" AS row FIELDTERMINATOR ' '
MERGE (from:City { name: row[0] })
MERGE (to:City { name: row[1] })
CREATE (from)-[:Flights { day: toInt(row[2]), cost: toInt(row[3]) } ]->(to);

# query
MATCH (from:City { name: "SHA"})-[f:Flights { day: 0 } ]->(to:City) RETURN to, f
